﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace PyvovarRoman.RobotChallange.Test
{ 
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);
            Assert.AreEqual(10, DistanceHelper.FindDistance(p1, p2));            
        }

        [TestMethod]
        public void TestFindPosition()
        {
            var map = new Map(6, 0);
            var stationPosition1 = new Position(15, 15);
            var stationPosition2 = new Position(15, 42);
            var stationPosition3 = new Position(15, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition1, RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition3, RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 15)},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 14)}
            };

            var robot = robots[1];
            Assert.AreEqual(stationPosition3, DistanceHelper.FindNearestFreeStation(robot, map, robots));
        }

        [TestMethod]
        public void TestFindNearest()
        {
            var map = new Map(6, 0);
            var stationPosition1 = new Position(15, 15);
            var stationPosition2 = new Position(15, 12);

            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition1, RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 10 });

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 20)}};
            var robot = robots[0];
          
            Assert.AreEqual(DistanceHelper.FindNearestStation(robot, map, robots), stationPosition1);
        }

        [TestMethod]
        public void TestFindNearestFreeStation()
        {
            var map = new Map(6, 0);
            var stationPosition1 = new Position(15, 15);
            var stationPosition2 = new Position(15, 12);

            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition1, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 14)}};
            var robot = robots[0];

            Assert.AreEqual(DistanceHelper.FindNearestFreeStation(robot, map, robots), stationPosition1);
        }

        [TestMethod]
        public void TestIsCellFree()
        {
            var map = new Map();
            var stationPosition1 = new Position(15, 15);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition1, RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 16)},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 14)},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 13)}
            };
            Assert.IsFalse(DistanceHelper.IsCellFree(stationPosition1, robots[1], robots));
        }

        [TestMethod]
        public void TestIsInCollectRange()
        {
            var map = new Map();
            var stationPosition1 = new Position(15, 15);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition1, RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(15, 16)},
                new Robot.Common.Robot() {Energy = 200, Position = new Position(20, 20)},
            };
            Assert.IsTrue(DistanceHelper.IsInCollectRange(robots[0].Position, stationPosition1));
            Assert.IsFalse(DistanceHelper.IsInCollectRange(robots[1].Position, stationPosition1));
        }
    }
}
    