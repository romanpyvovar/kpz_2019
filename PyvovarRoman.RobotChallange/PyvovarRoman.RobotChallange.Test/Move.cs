﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace PyvovarRoman.RobotChallange.Test
{
    [TestClass]
    public class Move
    {
        [TestMethod]
        public void TestMoveMethod()
        {
            var algorithm = new PyvovarRomanAlgorythm();
            var map = new Map();
            var stationPosition = new Position(1, 0);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                {new Robot.Common.Robot() {Energy = 9, Position = new Position(10, 0)}},
                {new Robot.Common.Robot() {Energy = 100, Position = new Position(20, 0)}}
            };
            var comand = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(comand is MoveCommand);
            Assert.AreEqual(((MoveCommand)comand).NewPosition, new Position(8,0));
        }
    }
}
