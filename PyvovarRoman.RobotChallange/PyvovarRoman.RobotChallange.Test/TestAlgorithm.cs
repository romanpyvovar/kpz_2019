﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace PyvovarRoman.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestCollect()
        {
            var algorithm = new PyvovarRomanAlgorythm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
               {new Robot.Common.Robot() {Energy = 200, Position = new Position(2, 2)}
            };

            var comand = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(comand is CollectEnergyCommand);          
        }

        [TestMethod]
        public void TestMove()
        {
            var algorithm = new PyvovarRomanAlgorythm();
            var map = new Map();
            var stationPosition = new Position(1, 0);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
               {new Robot.Common.Robot() {Energy = 200, Position = new Position(4, 0)}
            };

            var comand = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(comand is MoveCommand);
            Assert.AreEqual(((MoveCommand)comand).NewPosition, new Position(2, 0));
        }

        [TestMethod]
        public void TestCreate()
        {
            var algorithm = new PyvovarRomanAlgorythm();
            var map = new Map();
            var stationPosition = new Position(1, 0);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
               {new Robot.Common.Robot() {Energy = 500, Position = new Position(1, 0)}
            };

            var comand = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(comand is CreateNewRobotCommand);
        }
    }
}
