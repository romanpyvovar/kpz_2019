﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace PyvovarRoman.RobotChallange
{
    class Move
    {
        public static RobotCommand Faster(Position stationPosition)
        {
            return new MoveCommand() { NewPosition = stationPosition };
        }
        
        //---------------------------------------------------------------------
        public static RobotCommand Slow(Position stationPosition, Robot.Common.Robot myRobot, int multiplyer)
        {
            var directionX = Math.Sign(stationPosition.X - myRobot.Position.X);
            var directionY = Math.Sign(stationPosition.Y - myRobot.Position.Y);
            return new MoveCommand()
            {
                NewPosition = new Position()
                {
                    X = myRobot.Position.X + multiplyer * directionX, Y = myRobot.Position.Y + multiplyer * directionY
                }
            };
        }

        //---------------------------------------------------------------------
        public static RobotCommand MakeMove(Position stationPosition, Robot.Common.Robot myRobot)
        {
            int sX = stationPosition.X;
            int sY = stationPosition.Y;
            Position pos = new Position() { X = sX + 1, Y = sY };


            if (myRobot.Energy < DistanceHelper.FindDistance(stationPosition, myRobot.Position))
            {
                if(myRobot.Energy >= 100) 
                    return Slow(pos, myRobot, 3);
                return Slow(pos, myRobot, 2);
            }
            else
            {
                return Faster(pos);
            }
        }
    }
}
