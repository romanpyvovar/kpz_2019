﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace PyvovarRoman.RobotChallange
{
    public class PyvovarRomanAlgorythm : IRobotAlgorithm
    {
        public string Author => "Roman Pyvovar";
        public string Description => "Roman Pyvovar";
        public int RoundCount { get; set; }
        public void RomanPyvovarAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {

            var ListRobots = robots.Where(robot => robot.OwnerName == Author).ToList();
            var MyRobot = robots[robotToMoveIndex];
            int distance = 0;
            Position stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, ListRobots);
            Position notFreeStationPosition = DistanceHelper.FindNearestStation(robots[robotToMoveIndex], map, robots);

            if (DistanceHelper.IsInCollectRange(MyRobot.Position, stationPosition))
            {
                Position nextPosition = DistanceHelper.FindNextStation(robots[robotToMoveIndex], map, ListRobots);
                distance = DistanceHelper.FindDistance(MyRobot.Position, nextPosition);
            }

            if (MyRobot.Energy > 200 && DistanceHelper.IsInCollectRange(MyRobot.Position, stationPosition) && ListRobots.Count < 100)
            {
                CreateNewRobotCommand createNewRobotCommand = new CreateNewRobotCommand();
                createNewRobotCommand.NewRobotEnergy = 120;
                return createNewRobotCommand;
            }

            if (DistanceHelper.IsInCollectRange(MyRobot.Position, stationPosition))
                return new CollectEnergyCommand();

            else
            {
                return Move.MakeMove(stationPosition, MyRobot);
            }

            if (stationPosition == null)
                return null;
        }
    }
}
