﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace PyvovarRoman.RobotChallange
{
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        //---------------------------------------------------------------------
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        //---------------------------------------------------------------------
        public static Position FindNearestStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int d = FindDistance(station.Position, movingRobot.Position);
                if (d < minDistance)
                {
                    minDistance = d;
                    nearest = station;
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        //---------------------------------------------------------------------
        public static Position FindNextStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance && IsInCollectRange(movingRobot.Position, station.Position))
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        //---------------------------------------------------------------------
        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        //---------------------------------------------------------------------
        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (IsInCollectRange(robot.Position,cell))
                        return false;
                }
            }
            return true;
        }

       //---------------------------------------------------------------------
        public static bool IsInCollectRange(Position robot, Position station)
        {
            return Math.Abs(robot.X - station.X) <= 2 && Math.Abs(robot.Y - station.Y) <= 2;
        }
    }
}
